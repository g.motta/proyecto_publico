<?php
namespace App;

use Artisaninweb\SoapWrapper\Extension\SoapService;


class ServiceDesk extends SoapService {

    /**
     * @var string
     */
    protected $name = 'ServiceDesk';

    /**
     * @var string
     * http://200.6.53.70:8080/axis/services/USD_R11_WebService?WSDL => productiva
     */
    protected $wsdl = 'http://200.6.53.70:8080/axis/services/USD_R11_WebService?WSDL';
    
    /**
     * @var boolean
     */
    protected $trace = true;

    /**
     * Get all the available functions
     *
     * @return mixed
     */
    public function functions()
    {
        return $this->getFunctions();
    }

		public function login($params){
			$result = $this->call('login', [$params]);
			return $result;
		}

		public function getHandleForUserid($params){
			$result = $this->call('getHandleForUserid', [$params]);
			return $result;
		}

		public function createRequest($request){
			$result = $this->call('createRequest', [$request]);
			return $result;

		}

}
