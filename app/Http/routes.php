<?php

    use App\ServiceDesk;
    use App\RequestHandler;
    use App\Models\Alert;
    use App\Models\Company;
    use App\Models\Agent;
    use App\Models\Ticket;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/testt','TicketsController@ticketCaReegional');

/*Gestion de alerta manual.*/
Route::get('/manual/{id}/{os}/{mensaje}','TicketsController@marcarAlerta');

Route::get('/', 'AlertsController@index');
Route::get('/alertsdashboard', 'AlertsController@index');
Route::get('/alerts/get/{id}', 'AlertsController@getAlertById');
Route::get('/alerts/{state}/{client?}', 'AlertsController@searchAlerts');


Route::get('/alertss/{id?}', 'TicketsController@alertAutomatica');

Route::get('/clientes', 'AlertsController@cliente');


Route::get('/tickets', 'TicketsController@index');
Route::get('/tickets/all', 'TicketsController@getAll');
Route::post('/ticket/create', 'TicketsController@store');



Route::get('/tickets/search', 'AlertHistoryController@index');
Route::get('/ticket/viewSearch', 'AlertHistoryController@searchCustom');




Route::get('/servicedesk', function () {
    //Testing login

    $servicedesk = new ServiceDesk();

    $id =  $servicedesk->login([
            'username' => 'analista_dcc',
            'password' => 'Sonda.2016#',
            ]);

		$creatorHandler = $servicedesk->getHandleForUserid([
            'sid'     => $id->loginReturn,
            'userID' => 'dsciaraffia@quintec.cl',
        ]);


    $request = new RequestHandler($id->loginReturn, $creatorHandler->getHandleForUseridReturn);

    //dd($request->get());

    $data = "El notebook no prende";

    dd($servicedesk->createRequest($request->get($data)));

});

Route::get('/solarwindsreal', function(){

    $alerts = Alert::first();
    dd( $alerts );

});

Route::get('/solarwindsTickets', function(){

    $Ticket = Ticket::all();
    dd( count($Ticket) );

});

Route::get('/test', function(){
    $feriado = Feriado::all();
    dd($feriado);

});

//Creación de ticket clinica las condes.

Route::post('/Clinica','TicketsController@ticketClinicaCondes');


//cambio de valor en la cantidad de alerta que se puede gestionar
Route::get('/indicador/{cantidad}','AlertsController@changeTotal');
