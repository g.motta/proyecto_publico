<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ServiceDesk;

use App\CaServiceDeskRegional;

use App\Models\Alert;

use App\Models\AlertActive;

use App\Models\Ticket;

use App\RequestHandler;

use App\MoebiusDesk;

use DB;

use App\Models\Grupos;

use App\Models\AlertHistory;

use App\Models\usuarios;

use App\Models\AlertError;

use App\Models\TicketEmail;

use App\Models\IntegracionCA;

use App\ClienteCa;

class TicketsController extends Controller
{
    public function store(Request $request){



        $OsGenerada = "";
        $alert = Alert::find($request->get('id'));
        $gestionAutomatico = 0;
        json_decode($alert);

        $goCa = ClienteCa::where('cliente',$alert->NodoCliente)->first();

        if(is_null($goCa)){

          if(trim($alert->CIResponsable) === 'Cliente' || trim($alert->NodoResponsable) === 'Cliente')
          {
              //Entrara en condición pero si el CiReponsable  empieza con GSDC
              $CiResponsable = substr($alert->CIResponsable,0,4);
              if($CiResponsable === 'GSDC' || trim($alert->CIResponsable) === 'Sistemas Financieros Corfo')
              {
                  $OsGenerada = TicketsController::alertOther($alert,$gestionAutomatico);

                    return $OsGenerada;
              }else{
                  $OsGenerada = TicketsController::alertClient($alert,$gestionAutomatico);

                    return $OsGenerada;
              }

          }else
          {



              $OsGenerada = TicketsController::alertOther($alert,$gestionAutomatico);

              return $OsGenerada;

          }

      }else{
          $OsGenerada = TicketsController::ticketCaReegional($alert,$gestionAutomatico,$goCa);
          return $OsGenerada;
      }


    }


    public function alertClient($client,$tipoGestion)
    {

        $nombreBeneficiario = Usuarios::searchbeneficiary($client->NodoCliente);

        $serviceWeb = new MoebiusDesk();
        $OsPadre = 0;
        $OsGenerada = "";
            $mensaje =
                    ' Estimados se ha generado la alerta en solarwinds por el siguiente motivo: '. $client->Mensaje.
                    "\n".' el cual es referente a nuestro cliente : '. $client->NodoCliente .
                    "\n".' Fecha: ' . $client->Fecha .
                    "\n".' CI: ' . $client->CICaption .
                    "\n".' Responsable del CI :'. $client->CIResponsable.
                    "\n".' Nodo : ' . $client->NodoCaption.
                    "\n".' CITipo : ' . $client->CITipo.
                    "\n".' ID del nodo : ' . $client->NodoId.
                    "\n".' IP del nodo : ' . $client->IP.
                    "\n".' Responsable del Nodo : ' . $client->NodoResponsable.
                    "\n".' Poller : ' . $client->NodoPoller;

            $params = array(
                'intCodSistema' =>18,
                'codEmpresaExterna' =>$nombreBeneficiario['codEmpresaExterna'],
                'codUsuarioExterno' =>'CDM_GESTION DE EVENTOS',
                'codAlertaExterna' =>'DCC_'.$client->AlertActiveID,
                'descripcion' =>$mensaje,
                'codContrato' =>364,
                'codServicio' =>10368,
                'codCobertura' =>49937,
                'grupoResolutor' =>3590,
                'clasificacion' =>2242073,
                'impactedUsers' =>1,
                'codTipoOS' =>19,
            );

             $OsPadre = $serviceWeb->consumirServicio($params);
             $OsGenerada = $OsPadre;


             if($OsPadre > 0)
             {
                  $tickets = [
                    'system'    => 'MoebiusReport',
                    'system_id' => $OsPadre,
                    'gestion'   => $tipoGestion

                 ];

                $ticket = Ticket::generateOnDb($client, $tickets);
                $alertHistory = AlertHistory::generateHistory($client, $tickets);
                $mensajeOS = 'Se genero una OS en Moebius de Numero : ';
                AlertActive::Searchprocedure($client->AlertActiveID,$OsPadre,$mensajeOS);


             }else{

                        $tickets = [
                            'system'    => 'MoebiusReport',
                            'system_id' => $OsPadre,
                       ];


                        $alertHistory = AlertError::generateError($alert, $tickets);


                    }

             return $OsGenerada;


    }



    public function alertOther($alert,$tipoGestion)
    {

        if( trim($alert->NodoCliente) == 'Casa Ideas'){


                 $servicedesk = new ServiceDesk();
                 $login_data = ['username' => 'analista_dcc','password' => 'Sonda.2016#'];
                 $id = $servicedesk->login($login_data);

                if(trim($alert->NodoCliente) == 'Casa Ideas')
                {
                    $pcat= 1120998430; //Numero del grupo al cual se le asignara el ticket.
                    $token_data = ['sid' => $id->loginReturn,'userID' => 'casaideas.2016'];
                    $afectado = $servicedesk->getHandleForUserid($token_data);

                }

            $id = $servicedesk->login($login_data);

            $token_data = ['sid' => $id->loginReturn,'userID' => 'dsciaraffia@quintec.cl'];

            $creatorHandler = $servicedesk->getHandleForUserid($token_data);

                    if(empty($alert->RelatedNodeCaption)){
                         $valorFinal = $alert->NodoCaption;
                    }else{
                         $valorFinal = $alert->RelatedNodeCaption;
                    }

          $mensaje =
                    ' Estimados se ha generado la alerta en solarwinds por el siguiente motivo: '. $alert->Mensaje.
                    "\n".' el cual es referente a nuestro cliente : '. $alert->NodoCliente .
                    "\n".' Fecha: ' . $alert->Fecha .
                    "\n".' CI: ' . $alert->CICaption .
                    "\n".' Responsable del CI :'. $alert->CIResponsable.
                    "\n".' Nodo :'. $valorFinal.
                    "\n".' CITipo : ' . $alert->CITipo.
                    "\n".' ID del nodo : ' . $alert->NodoId.
                    "\n".' IP del nodo : ' . $alert->IP.
                    "\n".' Responsable del Nodo : ' . $alert->NodoResponsable.
                    "\n".' Poller : ' . $alert->NodoPoller;

            $request = new RequestHandler($id->loginReturn, $creatorHandler->getHandleForUseridReturn,$afectado->getHandleForUseridReturn,$pcat);




            $searchDown = Alert::searchDown($alert->Mensaje); //if Mensaje say down return true

            if($alert->Severidad == 2 and $searchDown == true)
            {

                $caTicket = $servicedesk->createRequest($request->incident($mensaje));
            }else
            {

                $caTicket = $servicedesk->createRequest($request->get($mensaje));
            }


            //Dummy Data
            $tickets = [
                'system'    => 'ServiceDesk',
                'system_id' => $caTicket->newRequestNumber,
                'gestion'   => $tipoGestion

            ];


            //If que validara la creacion del ticket.
            if($caTicket->newRequestNumber > 0)
            {
                $ticket = Ticket::generateOnDb($alert, $tickets);
                $alertHistory = AlertHistory::generateHistory($alert, $tickets);
                $mensajeOS = 'Se genero un Ticket en Service Desk de Numero : ';
                AlertActive::Searchprocedure($alert->AlertActiveID,$caTicket->newRequestNumber,$mensajeOS);
            }else{

                        $tickets = [
                            'system'    => 'MoebiusReport',
                            'system_id' => $OsPadre,
                       ];


                        $alertHistory = AlertError::generateError($alert, $tickets);


            }

            return $caTicket->newRequestNumber;



        }else
        {

                $OsGenerada = "";
                $OsPadre = 0;
                $serviceWeb = new MoebiusDesk();
                $codigo = Grupos::ObtenerCodigo($alert->CIResponsable,$alert->NodoResponsable);
                $nombreBeneficiario = Usuarios::searchbeneficiary($alert->NodoCliente);
                $searchDown = Alert::searchDown($alert->Mensaje);

                if(empty($alert->RelatedNodeCaption)){
                         $valorFinal = $alert->NodoCaption;
                }else{
                         $valorFinal = $alert->RelatedNodeCaption;
                }


                $mensaje =
                    ' Estimados se ha generado la alerta en solarwinds por el siguiente motivo: '. $alert->Mensaje.
                    "\n".' el cual es referente a nuestro cliente : '. $alert->NodoCliente .
                    "\n".' Fecha: ' . $alert->Fecha .
                    "\n".' CI: ' . $alert->CICaption .
                    "\n".' Responsable del CI :'. $alert->CIResponsable.
                    "\n".' Nodo : ' . $valorFinal.
                    "\n".' CITipo : ' . $alert->CITipo.
                    "\n".' ID del nodo : ' . $alert->NodoId.
                    "\n".' IP del nodo : ' . $alert->IP.
                    "\n".' Responsable del Nodo : ' . $alert->NodoResponsable.
                    "\n".' Poller : ' . $alert->NodoPoller;


                   if($alert->Severidad == 2 and $searchDown == true)
                    {

                        $params = array(
                                'intCodSistema' =>18,
                                'codEmpresaExterna' =>$nombreBeneficiario['codEmpresaExterna'],
                                'codUsuarioExterno' =>'CDM_GESTION DE EVENTOS',
                                'codAlertaExterna' =>'DCC_'.$alert->AlertActiveID,
                                'descripcion' =>$mensaje,
                                'codContrato' =>364,
                                'codServicio' =>10368,
                                'codCobertura' =>49937,
                                'grupoResolutor' =>3479,
                                'clasificacion' =>2242073,
                                'impactedUsers' =>1,
                                'codTipoOS' =>1, //Cambiar a 1 para generar como incidente
                            );

                        $OsPadre = trim($serviceWeb->consumirServicio($params));

                               $datos = array(
                                      'idOSFather'         => $OsPadre,
                                      'UserRequestingName' => 'Evelyn Silva Castro',
                                      'userBeneficiaryName'=> 'Evelyn Silva Castro',
                                      'description'        => $mensaje,
                                      'idContract'         => 364,
                                      'idService'          => 10368,
                                      'idCoverage'         => 49937,
                                      'solutionGroupName'  => $codigo['grupo'],
                                      'nameAgent'          => $codigo['agenda'],
                                      'idSituation'        => 0,
                                      'codTipoOS'          => 1,
                                );
                        $osComplementaria = $serviceWeb->generateOsComplementary($datos);

                       $OsGenerada = "Os padre generada : " . $OsPadre . " y su complementaria es : " . $osComplementaria;


                    }else
                    {
                                $params = array(
                                'intCodSistema' =>18,
                                'codEmpresaExterna' =>$nombreBeneficiario['codEmpresaExterna'], //Generico pero vigente
                                'codUsuarioExterno' =>'CDM_GESTION DE EVENTOS',
                                'codAlertaExterna' =>'DCC_'.$alert->AlertActiveID,
                                'descripcion' =>$mensaje,
                                'codContrato' =>364,
                                'codServicio' =>10368, //en productivo es : 10368    y en qua 10353
                                'codCobertura' =>49937, //QA: 48764   -> Productiva :49937
                                'grupoResolutor' =>$codigo['codigo'],
                                'clasificacion' =>2242073, //No influye
                                'impactedUsers' =>1,
                                'codTipoOS' =>19,
                            );

                          $OsPadre = $serviceWeb->consumirServicio($params);
                          $OsGenerada = $OsPadre;


                    }



                 if($OsPadre > 0 )
                    {

                        $tickets = [
                            'system'    => 'MoebiusReport',
                            'system_id' => $OsPadre,
                            'gestion'   => $tipoGestion
                       ];

                        $ticket = Ticket::generateOnDb($alert, $tickets);
                        $alertHistory = AlertHistory::generateHistory($alert, $tickets);
                        $mensajeOS = 'Se genero una OS en Moebius de Numero : ';
                        AlertActive::Searchprocedure($alert->AlertActiveID,$OsGenerada,$mensajeOS);

                    }else{

                        $tickets = [
                            'system'    => 'MoebiusReport',
                            'system_id' => $OsPadre,
                       ];


                        $alertHistory = AlertError::generateError($alert, $tickets);


                    }

                return $OsGenerada;
        }

    }

    public function getAll(){

         $tickets = Ticket::orderBy('created_at','desc')->take(50)->get();

        return response()->json($tickets);
    }

    public function index(){
        return view('tickets.index');
    }


    public function marcarAlerta($alertid,$os,$mensaje){

        AlertActive::Searchprocedure($alertid,$os,$mensaje);

    }


    public function ticketClinicaCondes(Request $request){

        $codigo = 12;
	$grupo = 227;
        if($request->tipo === "INCIDENTE"){
		$codigo = 1;
		$grupo = 227;
        }else if($request->tipo === "REQUERIMIENTO"){
	        $codigo = 12;
		$grupo = 227;
        }

        $serviceWeb = new MoebiusDesk();
        //$numeroOs = rand(1000000,10000000);
        $indice = TicketEmail::getID();

        $params = array(
                                'intCodSistema' =>18,
                                'codEmpresaExterna' =>'0093930000-7',
                                'codUsuarioExterno' =>'CDM_GESTION DE EVENTOS',
                                'codAlertaExterna' =>'DCC_CLC'.$indice,
                                'descripcion' =>$request->info,
                                'codContrato' =>364,
                                'codServicio' =>10368,
                                'codCobertura' =>49937,
                                'grupoResolutor' =>$grupo,
                                'clasificacion' =>2242073,
                                'impactedUsers' =>1,
                                'codTipoOS' =>$codigo, //Cambiar a 1 para generar como incidente
                            );

        $numeroOs = trim($serviceWeb->consumirServicio($params));

       if($numeroOs > 0)
        {
            $retorno = "Ok;".$numeroOs;
            TicketEmail::guardar($request,$numeroOs);
        }else{
             $retorno = "Error; No se pudo generar una orden de servicios.";
        }

        return $retorno;

    }


    public function Automatic($id){



        $OsGenerada = "";
        $alert = Alert::find($id);
        $gestionAutomatico = 1;
        json_decode($alert);

        $goCa = ClienteCa::where('cliente','like','%'.$request->NodoCliente.'%')->first();

        if(is_null($goCa)){

        if(trim($alert->CIResponsable) === 'Cliente' || trim($alert->NodoResponsable) === 'Cliente' and  trim($alert->NodoCliente) != 'Casa Ideas' || trim($alert->NodoCliente) != 'La Polar Chile')
        {
            //Entrara en condición pero si el CiReponsable  empieza con GSDC
            $CiResponsable = substr($alert->CIResponsable,0,4);
            if($CiResponsable === 'GSDC' || trim($alert->CIResponsable) === 'Sistemas Financieros Corfo')
            {
                $OsGenerada = TicketsController::alertOther($alert,$gestionAutomatico);
                return $OsGenerada;
            }else{
                $OsGenerada = TicketsController::alertClient($alert,$gestionAutomatico);
                return $OsGenerada;
            }

        }else
        {

            $OsGenerada = TicketsController::alertOther($alert,$gestionAutomatico);

            return $OsGenerada;

        }

      }else{
           $OsGenerada = TicketsController::ticketCaReegional($alert,$gestionAutomatico,$goCa);
           return $OsGenerada;
      }

    }




    public function ticketCaReegional($alert,$tipoGestion,$datosCliente){

        $caRegional = new CaServiceDeskRegional();
        $numeroTicket = 0;
        $resultado = "";
        $datosIntegracion = IntegracionCA::first();

        
        if(empty($alert->RelatedNodeCaption)){
             $valorFinal = $alert->NodoCaption;
        }else{
             $valorFinal = $alert->RelatedNodeCaption;
        }

        $mensaje =
                  ' Estimados se ha generado la alerta en solarwinds por el siguiente motivo: '. $alert->Mensaje.
                  "\n".' el cual es referente a nuestro cliente : '. $alert->NodoCliente .
                  "\n".' Fecha: ' . $alert->Fecha .
                  "\n".' CI: ' . $alert->CICaption .
                  "\n".' Responsable del CI :'. $alert->CIResponsable.
                  "\n".' Nodo :'. $valorFinal.
                  "\n".' CITipo : ' . $alert->CITipo.
                  "\n".' ID del nodo : ' . $alert->NodoId.
                  "\n".' IP del nodo : ' . $alert->IP.
                  "\n".' Responsable del Nodo : ' . $alert->NodoResponsable.
                  "\n".' Poller : ' . $alert->NodoPoller;

         //  $grupo = IntegracionCA::getGrupo($alert->CIResponsable,$alert->NodoResponsable);
          $searchDown = Alert::searchDown($alert->Mensaje); //if Mensaje say down return true
          $tipoSeveridad = "R";

          if($alert->Severidad == 2 and $searchDown == true)
          {
            $tipoSeveridad = "I";
          }

              $params = array(
                'user' => "SolarwindChile@sndintca.cl",
                'password' => "SolarwindChile2017",
                'idtenant' => $datosCliente->tenant,
                'idsolictante' => $datosCliente->id_afectado,
                'idafectado' =>  $datosCliente->id_afectado,
                'idorigen' => $datosCliente->id_reporte,
                'idcategory' => $datosCliente->id_categoria,
                'idgrupo' =>  $datosIntegracion->id_grupo_habil,
                'idassignee' => '?',
                'description' => $mensaje,
                'tipo' => $tipoSeveridad,

              );


        $numeroTicket = $caRegional->CreateTicket($params);


        if(is_null($numeroTicket->codError)){
          $resultado = $numeroTicket->serviceOrderId[0]->idTicket;
        }else{
          $resultado = $numeroTicket->codError;
        }


        if(is_null($numeroTicket->codError))
           {

               $tickets = [
                   'system'    => 'CaRegional',
                   'system_id' => $numeroTicket->serviceOrderId[0]->idTicket,
                   'gestion'   => $tipoGestion
              ];

               $ticket = Ticket::generateOnDb($alert, $tickets);
               $alertHistory = AlertHistory::generateHistory($alert, $tickets);
               $mensajeOS = 'Se genero una Ticket en CA Regional de Numero : '. $numeroTicket->serviceOrderId[0]->idTicket;
               AlertActive::Searchprocedure($alert->AlertActiveID,$numeroTicket->serviceOrderId[0]->idTicket,$mensajeOS);

           }else{

               $tickets = [
                   'system'    => 'CaRegional',
                   'system_id' => $numeroTicket->codError,
              ];


               $alertHistory = AlertError::generateError($alert, $tickets);


           }

           return $resultado;

    }

}
