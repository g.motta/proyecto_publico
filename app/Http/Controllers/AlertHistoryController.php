<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Collection as Collection;

use App\Models\AlertHistory;

use App\Models\Moebius;

use Carbon\Carbon;

use DB;

class AlertHistoryController extends Controller
{


	public function index()
	{

		$alertHistory = AlertHistory::orderBy('alert_id','asc')->take(0)->get();

		return view('tickets.viewSearch')->with('AlertHistory',$alertHistory);

	}


	public function searchCustom(Request $request)
	{


		if($request->fechaHasta == "")
		{
			$fecha = date('Y-m-j');
			$fecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
			$fecha = date ( 'Y-m-j' , $fecha );
		}else
		{
			$fecha = strtotime ( '+1 day' , strtotime ( $request->fechaHasta ) ) ;
			$fecha = date ( 'Y-m-j' , $fecha );

		}


		$alertHistory = AlertHistory::where('created_at', '>=', $request->fechaDesde)
						->select()
	   					->where('created_at','<=', $fecha)
	   					->where($request->choise,'like','%'.trim($request->choiseSeleccion).'%')
	   					->get();

	  	for ($i=0; $i < count($alertHistory) ; $i++) {

	  		if($alertHistory[$i]->systema === 'MoebiusReport'){
	  		 $resultado[$i] = Moebius::getosMoebius($alertHistory[$i]->system_id);
	 		   $alertHistory[$i]->alertObjectID = "";
	  		 $alertHistory[$i]->alertObjectID = $resultado[$i];
	  		 $alertHistory[$i]->created_at = Carbon::parse($alertHistory[$i]->created_at)->subMinute(1)->subSecond(24);
	  		}else{

	  			 $alertHistory[$i]->alertObjectID = "Ticket de CA.";
	  		}


	    }




	     return view('tickets.viewSearch')->with('AlertHistory',$alertHistory);
	 }

}
