<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Alert;

use App\Http\Requests;

use Illuminate\Support\Collection as Collection;

use DB;
use App\Feriado;
use App\Models\CantidadAlerta;
use App\Models\ClientesExcluidos;
use App\Models\LogAlert;
use App\Http\Controllers\TicketsController;

class AlertsController extends Controller
{

    public function __construct(Alert $alert){

        $this->alert = $alert;
        DB::setDefaultConnection('sqlsrv');

    }

    public function index(){
        return view('alerts.index');

   }

    /**
     * Search alerts into Solarwind database
     * @param  [type] $client [description]
     * @param  [type] $state  [description]
     * @return [type]         [description]
     */
    public function searchAlerts($state, $clients = null){



        $clients = null;


        $severity = 2;

        $alerts = $this->alert->getAlerts($state, $clients, 1000, $severity);



        return $alerts;

    }

    public static function automaticAlert(){
        $mensaje ="";
        $feriado = Feriado::where('Dia','=',date('Y-m-d'))->first();
        $horaActual = date('H:i:s');
        $ticket = new TicketsController();
        $alert = new Alert;
        $cantidadAlertas = CantidadAlerta::where('id','=',1)->first();

        $clientesExcluidos = ClientesExcluidos::all();

        for ($i=0; $i < count($clientesExcluidos) ; $i++) {
            $clientss[$i] = $clientesExcluidos[$i]->name;
        }

        $alertTodas = $alert->getAllAlert($clientss);


        if($cantidadAlertas->cantidad === 0){
          $mensaje = "La gestión de alertas automática se encuentra deshabilitada";
        }else{
          
          if (empty($feriado) and $horaActual >= "08:45" and $horaActual <= "18:15") {
            if(count($alertTodas) <= $cantidadAlertas->cantidad){
              for ($i=0; $i < count($alertTodas) ; $i++) {
                $ticket->Automatic($alertTodas[$i]->AlertActiveID);
              }
  
              $mensaje = "Las alertas se gestionaron de manera exitosa";
              
            }else{
              $mensaje = "Las alertas de Solarwinds superan el limite permitido de ".$cantidadAlertas->cantidad;
  
            }
  
          }else{
            if(empty($feriado)){
              $mensaje = "No se gestionaron las alarmas debido a que la hora no está en el rango de horario hábil.";
            }else{
              $mensaje = "No se gestionaron las alarmas debido a que el día es feriado.";
            }
  
          }
        }
        

        $informacion = [
          'limitAlert'    => $cantidadAlertas->cantidad,
          'cantSolarwinds' => count($alertTodas),
          'descripcion' => $mensaje,
       ];
       LogAlert::createLog($informacion);


    }






     public function searchAlertStatus($status){


        $alerts = $this->alert->getAlerts($status, $clients, 100, $severity);

         foreach ($alerts as $alert) {

            $alert['client'] = $alert->NodoCliente;
         }

        return $alerts;

        $alerts = Alert::All();
        dd($alerts);
    }


    public function getAlertByID($AlertActiveID){

        $alert = Alert::all()->where('AlertActiveID',$AlertActiveID);


        $collection = Collection::make($alert);
        return $collection->first();

    }


    public function cliente(){

    	return view('alerts.client');

    }


    public function changeTotal($nuevaCantidad){

        $cantidadAlarmas = CantidadAlerta::first();

        $cantidadAlarmas->cantidad = $nuevaCantidad;
        $cantidadAlarmas->save();

    }



}
