<?php
namespace App;

use Artisaninweb\SoapWrapper\Extension\SoapService;
use XmlParser;

class CaServiceDeskRegional extends SoapService {

    /**
     * @var string
     */
    protected $name = 'CaServiceDeskRegional';

    /**
     * @var string
     *
     */
    //QA protected $wsdl = 'http://tstintegration.sonda.com/Integration.WebServices.ITSM/?WSDL'
    protected $wsdl = 'https://webintegration.sonda.com/Integration.WebServices.ITSM/?WSDL';

    /**
     * @var boolean
     */
    protected $trace = true;

    /**
     * Get all the available functions
     *
     * @return mixed
     */
    public function functions()
    {
        return $this->getFunctions();
    }



		public function CreateTicket($request){

			$result = $this->call('CreateTicket', [$request]);



      $xml = XmlParser::extract($result->CreateTicketResult->any);

             $response = $xml->parse([
                        'serviceOrderId' => ['uses' => 'resultado.template[::idTicket]'],
                        'codError' => ['uses' => 'mensajeError'],
                ]);


              

        return json_decode(str_replace("::idTicket","idTicket",json_encode($response)));



		}

}
