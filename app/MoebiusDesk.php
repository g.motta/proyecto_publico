<?php
namespace App;

use Artisaninweb\SoapWrapper\Extension\SoapService;


class MoebiusDesk extends SoapService {

    /**
     * @var string
     */
    protected $name = 'MoebiusDesk';

    /**
     * @var string
        *URL: moebius.sonda.com => Production
        
     */
 
     //protected $wsdl = 'http://10.164.169.20/Moebius.Services.IncidentManagement/WSIncident.asmx?WSDL';  // => prueba
     protected $wsdl = 'http://moebius.sonda.com/moebius.services.IncidentManagement/WSIncident.asmx?WSDL';
     //rotected $wsdl = "http://moebius.sonda.com/moebius.services.IncidentManagement/WSIncident.asmx?WSDL";



    /**s
     * @var boolean
     */
    protected $trace = true;

    /**
     * Get all the available functions
     *
     * @return mixed
     */
    public function functions()
    {
        return $this->getFunctions();
    }

		
        //funcion encargada de generar el webservice.
		public function consumirServicio($params){
			$result = $this->call('CreateServiceOrder', [$params]);
            $resultado = json_encode($result, false);
            if (!$resultado) {
                    echo "falso. <br>";
                    echo $resultado;
                } else {
                    //Voy recortando y haciendo filas de todos los String que tengan /"
                    $array = explode ("\"", $resultado);
                    //En la fila 4 -> segun array, saco todo los string y dejo solo loos numeros obteniendo asi las OS.
                    $OS = intval(preg_replace('/[^0-9]+/', '', $array[4]), 10);
                    
                    return $OS;
                    
                }
		}


        //funcion encargada de generar el webservice.
        public function generateOScomple($params){
            $result = $this->call('CreateServiceOrder', [$params]);
            $resultado = json_encode($result, false);
            if (!$resultado) {
                    echo "falso. <br>";
                    echo $resultado;
                } else {
                    //Voy recortando y haciendo filas de todos los String que tengan /"
                    $array = explode ("\"", $resultado);
                    //En la fila 4 -> segun array, saco todo los string y dejo solo loos numeros obteniendo asi las OS.
                    $OS = intval(preg_replace('/[^0-9]+/', '', $array[4]), 10);
                    
                    return $OS;
                    
                }
        }


      //funcion encargada de generar el webservice.
        public function generateOsComplementary($params)
        {
            $result = $this->call('CreateComplementaryOS', [$params]);
            $resultado = json_encode($result, false);
                if (!$resultado) 
                {
                    echo "falso. <br>";
                    echo $resultado;
                } else 
                {
                    //Voy recortando y haciendo filas de todos los String que tengan /"
                    $array = explode ("\"", $resultado);
                    //En la fila 4 -> segun array, saco todo los string y dejo solo loos numeros obteniendo asi las OS.
                    $OS = intval(preg_replace('/[^0-9]+/', '', $array[4]), 10);
                    
                    return $OS;
                    
                }
        }
    	

}
