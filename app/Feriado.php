<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feriado extends Model
{
    protected $connection = 'mysql';
    protected $table = 'feriados';
    public $timestamps = true;
}
