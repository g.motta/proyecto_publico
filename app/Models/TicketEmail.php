<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use DB;
class TicketEmail extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tickets_email';
    public $timestamps = true;
    protected $primaryKey = 'id';


    public static function guardar($request,$numeroOs){

    	$ticket = new TicketEmail;
    	$ticket->message = $request->info;
    	$ticket->tipo_orden = $request->tipo;
    	$ticket->numero_os = $numeroOs;
    	$ticket->created_at = date("Y-m-d H:i:s");
    	$ticket->updated_at = date("Y-m-d H:i:s");

    	$resultado = $ticket->save();

    	return $resultado;
    }


    public static function getID(){

    	$valor = TicketEmail::max('id');
    	
    	$resultadoFinal = $valor +1;
    
    	return $resultadoFinal;
    }
}
