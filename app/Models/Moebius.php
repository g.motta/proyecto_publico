<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

use Carbon\Carbon;

class Moebius extends Model
{
    protected $connection = 'external_connexion';
    protected $table = 'tb_chamado';
    protected $primaryKey = 'pk_int_codigo_chamado';





    public static function getOsMoebius($alertas){

		$fechaRestada = "";
    	$resultado = Moebius::where('pk_int_codigo_chamado','=',$alertas)->select('dtt_atendimento_final_realizado')->first();

    	//dd($resultado. "  alerta->".  $alertas );
        if(!is_null($resultado)){
             $fechaRestada = Carbon::parse($resultado['dtt_atendimento_final_realizado'])->subHour(3);
        }


    	return $fechaRestada;

    }


}
