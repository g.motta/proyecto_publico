<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientesExcluidos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'clienteExcluido';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
}
