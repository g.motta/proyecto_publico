<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Ticket
 * @package App\Models
 */
class AlertError extends Model
{
    protected $connection = 'mysql';
    protected $table = 'alertError';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
 




    
   	public static function generateError($alert, $ticket){
   		
      
      $mensaje =  
                    ' Fecha: ' . $alert->Fecha . 
                    "\n".' CI: ' . $alert->CICaption . 
                    "\n".' Responsable del CI :'. $alert->CIResponsable.
                    "\n".' Nodo : ' . $alert->NodoCaption.
                    "\n".' CITipo : ' . $alert->CITipo.
                    "\n".' ID del nodo : ' . $alert->NodoId.
                    "\n".' IP del nodo : ' . $alert->IP.
                    "\n".' Responsable del Nodo : ' . $alert->NodoResponsable.
                    "\n".' Poller : ' . $alert->NodoPoller;   

   		$data = [
          'systema'           => $ticket['system'],
          'client'            => $alert->NodoCliente,
          'Nodo'              => $alert->NodoId,
          'ip'                => $alert->IP,
					'grupoNodo'         => $alert->NodoResponsable,
          'grupoCi'           => $alert->CIResponsable,
          'alert_time_raised' => $alert->Fecha,
          'alert_id'          => $alert->AlertActiveID,
          'system_id'         => $ticket['system_id'],
         	'alertObjectID'     => $alert->AlertObjectID,
          'mensaje'           => $mensaje,
      ];

   		$alertHistory = self::create($data);
			

   		
   		return $alertHistory;

   	}
}
