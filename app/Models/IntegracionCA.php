<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Feriado;
use App\Models\AlertActive;

class IntegracionCA extends Model
{
    protected $connection = 'mysql';
    protected $table = 'caIntegration';
    protected $guarded = ['id'];
    public $timestamps = false;


    public static function getGrupo($valor1,$valor2){


      $CiResponsable = substr($valor1,0,4);
      $NodoResponsable = substr($valor2,0,4);
      $grupo_defecto = 'E1A58A71C585204BA10804CB53D372AA'; //Al no encontrar nada se envia este codigo por defecto.
      $responsable_defecto = "";
      $diaActual = date('Y-m-d'); // Obtendra el dia actual
      $diaHabil = AlertActive::sumasdiasemana($diaActual);//Funcion que devovlera solo los dias habiles
      $horaActual = date('H:i:s'); // obtiene la hora actual
      $feriado = Feriado::where('Dia','=',$diaActual)->first(); //Busca si la efcha actual es un dia feriado
      $valores = IntegracionCA::where('nombre_grupo_solarwinds','=',$valor1)->first();

      //Si no se encuentra ningun grupo.
      if(empty($valores)){
        $grupos = [
          'id_grupo'       => $grupo_defecto,
          'id_responsable' => $responsable_defecto,
        ];
        return $grupo;
      }else{
        if($diaActual == $diaHabil and $horaActual >= $valores->start_date and $horaActual <= $valores->finish_date and empty($feriado))
        {
              $grupos = [
                'id_grupo'        => $valores->id_grupo_habil,
                'id_responsable'  => $valores->id_agente_habil,

              ];
              return $grupo;
        }else{
              $grupos = [
                'id_grupo'        => $valores->id_grupo_inhabil,
                'id_responsable'  => $valores->id_agente_inhabil,

              ];
              return $grupo;
        }
      }

    }

}
