<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CantidadAlerta extends Model
{
    protected $connection = 'mysql';
    protected $table = 'cantidadAlertas';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;


}
