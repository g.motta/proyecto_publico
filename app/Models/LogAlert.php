<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class LogAlert extends Model
{
  protected $connection = 'mysql';
  protected $table = 'logAlert';
  protected $primaryKey = 'id';
  public $timestamps = true;


  public static function createLog($log){
      
        $newLog = new LogAlert();

        $newLog->limitAlert         = $log['limitAlert'];
        $newLog->cantSolarwinds     = $log['cantSolarwinds'];
        $newLog->descripcion        = $log['descripcion'];
        $newLog->created_at         = date("Y-m-d H:i:s");
        $newLog->updated_at         = date("Y-m-d H:i:s");
        $newLog->save();
  }

}
