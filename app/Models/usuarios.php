<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use DB;
/**
 * Class Ticket
 * @package App\Models
 */
class Usuarios extends Model
{
    protected $connection = 'mysql';
    protected $table = 'external_clients';
    protected $primaryKey = 'ID';
    public $timestamps = false;



	    public static function searchbeneficiary($name){


	    	$codBeneficiary = Usuarios::where('NombreSolarwinds','=',$name)->first();
	      

	      if($codBeneficiary['codEmpresaExterna'] == null)
	      {
	      	$codBeneficiary['codEmpresaExterna'] = '0083628100-4';
	      }
	      
	      return $codBeneficiary;

	   }
}