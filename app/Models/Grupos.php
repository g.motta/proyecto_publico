<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use App\Models\AlertActive;
use DB;
use App\Feriado;

class Grupos extends Model
{

	protected $connection = 'mysql';
    protected $table = 'grupo';
    public $timestamps = false;
    protected $primaryKey = 'id';



    public static function ObtenerCodigo($valor1,$valor2)
    {
        $CiResponsable = substr($valor1,0,4);
        $NodoResponsable = substr($valor2,0,4);
        $defecto = '3568'; //Al no encontrar nada se envia este codigo por defecto.
        $diaActual = date('Y-m-d'); // Obtendra el dia actual
        $diaHabil = AlertActive::sumasdiasemana($diaActual);//Funcion que devovlera solo los dias habiles
      	$horaActual = date('H:i:s'); // obtiene la hora actual
      	$feriado = Feriado::where('Dia','=',$diaActual)->first(); //Busca si la efcha actual es un dia feriado
		    $valores = Grupos::where('nombre','=',$valor1)->first();
		if($valores != null  ||  $valor1 !== "Sin Responsable" && !empty($valores))
       	{

		    if(!empty($valores))
		    {

			    if($diaActual == $diaHabil and $horaActual >= $valores->start_date and $horaActual <= $valores->finish_date and empty($feriado))
			    {

			    		$grupos = [
				    		'codigo' => $valores->habil,
				    		'grupo'  => $valores->grupoHabil,
				    		'agenda' => $valores->AgendaHabil,
				    	];
				    	return $grupos;
			    }else
			    {
			    	$grupos = [
				    		'codigo' => $valores->inhabil,
				    		'grupo'  => $valores->grupoInhabil,
				    		'agenda' => $valores->AgendaInhabil,
				    	];
				    	return $grupos;
			    }
		    }else
		    {
				 	$valores = Grupos::where('habil','=','3568')->first();
					$grupos = [
						 'codigo' => $defecto,
						 'grupo'  => $valores->grupoHabil,
						 'agenda' => $valores->AgendaHabil,
					 ];

				return $grupos;
		    }

	    }else
		{
			$valores = Grupos::where('nombre','=',$valor2)->first();
			if($valores != null)
			{


			   if(!empty($valores))
			   {
				   if($diaActual == $diaHabil and $horaActual >= $valores->start_date and $horaActual <= $valores->finish_date and empty($feriado))
				   {
						$grupos = [
				    		'codigo' => $valores->habil,
				    		'grupo'  => $valores->grupoHabil,
				    		'agenda' => $valores->AgendaHabil,
				    	];
				    	return $grupos;
			   	   }else
			   	   {
						$grupos = [
				    		'codigo' => $valores->inhabil,
				    		'grupo'  => $valores->grupoInhabil,
				    		'agenda' => $valores->AgendaInhabil,
				    	];
				    	return $grupos;
				   }
			   }else
			   {

					 	$valores = Grupos::where('habil','=','3568')->first();
					 	$grupos = [
				    		'codigo' => $defecto,
				    		'grupo'  => $valores->grupoHabil,
				    		'agenda' => $valores->AgendaHabil,
				    	];
						return  $grupos;
			   }
		   	}else{

		   			 	$valores = Grupos::where('habil','=','3565')->first();
		   			 	$grupos = [
				    		'codigo' => '3565',
				    		'grupo'  => $valores->grupoHabil,
				    		'agenda' => $valores->AgendaHabil,
				    	];

				return $grupos;
		   	}

		}




    }


}
