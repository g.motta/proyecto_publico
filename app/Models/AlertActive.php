<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use DB;


class AlertActive extends Model
{
    
    protected  $connection = 'sqlsrv';
    //protected $table = 'sMCC_UpdateStateAlertActive';
   
 
    public static function Searchprocedure($AlertActiveID,$osNum,$mensaje)
    {   
        
  		$nameSystem = "A.S.A Ticket";   
        $osNumero = $mensaje. $osNum;
        DB::statement('exec sMCC_UpdateStateAlertActive ?,?,?',array($AlertActiveID,$nameSystem,$osNumero));
       
    }


	public static function sumasdiasemana($fecha)
	{
		$dias = 0;
		$datestart= strtotime($fecha);
		$datesuma = 15 * 86400;
		$diasemana = date('N',$datestart);
		$totaldias = $diasemana+$dias;
		$findesemana = intval( $totaldias/5) *2 ; 
		$diasabado = $totaldias % 5 ; 
		if ($diasabado==6) $findesemana++;
		if ($diasabado==0) $findesemana=$findesemana-2;

		$total = (($dias+$findesemana) * 86400)+$datestart ; 
		return $twstart=date('Y-m-d', $total);
	}
				

}
