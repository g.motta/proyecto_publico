<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use DB;

/**
 * Class Alert
 * @package App\Models
 */
class Alert extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'vDCC_MC_ActiveAlerts';
    public $timestamps = false;
    protected $primaryKey = 'alertactiveid';


    //TODO: Change function name
    /**
     * This function process the xml data inside an alert
     * @return array data
     */


    public function getDataBodyAttribute(){

    	$xml = simplexml_load_string($this->data);

    	$lines = explode( PHP_EOL , $xml->sourceMessage->msgProperty[4]);

    	foreach ($lines as $line) {

    		if( strlen($line) > 1){

                if(strpos($line, ':')){

                    $words = explode(":", $line, 2);

    		        $array[$words[0]] = $words[1];
                }
                else{
                    $array[] = $line;
                }
			}
    	}

    	return $array;
    }


    public function getDataTextAttribute(){

        $xml = simplexml_load_string($this->data);

        $text = $xml->sourceMessage->msgProperty[4];

        return $text;

    }

    /**
     * Return client from message
     * @return string client
     */
    public function getClientAttribute(){

        $client = explode( '|' , $this->message);

        return $client[0];

    }

    public function getAlerts($state, $clients, $limit, $severity){

        $alerts = $this->whereNull('Atendida');


        if(isset($clients)){

            $alerts->where(function ($query) use ($clients){
                foreach($clients as $client){
                    $query->where('NodoCliente', '!=',$client);
                }
            });

        }

        if(trim($state) === 'cliente'){
            $alerts = DB::select(" SELECT * from vDCC_MC_ActiveAlerts
            where
            Mensaje not like '%_CA - Alert me when the free space of a volume is less than 15% New%'
            and Mensaje not like '%_CA - Custom alerts of warning volume was triggered.%'
            and CICaption not like  '%Windows Time Skew%'
            and CICaption not like '%Monitoreo detect_process%'
            and NodoResponsable not like '%Etapa Proyecto%'
	    and (NodoResponsable   like '%Cliente%'  or  CIResponsable  like '%Cliente%') ");

        }else
        {
            $alerts = $alerts->select(['NodoCliente', 'Severidad', 'CITipo', 'Mensaje', 'Fecha', 'AlertActiveID','IP','NodoCaption','NodoPoller','NodoResponsable','CIResponsable','CICaption'])
            ->take(2000)->orderBy('Fecha','desc')
            ->where('Mensaje','!=','_CA - Alert me when the free space of a volume is less than 15% New')
            ->where('Mensaje','!=','_CA - Custom alerts of warning volume was triggered.')
            ->where('CICaption','!=','Windows Time Skew')
            ->where('CICaption','!=','Monitoreo detect_process')
            ->where([
                    ['NodoResponsable','!=','Cliente'],
                    ['CIResponsable','!=','Cliente'],
                ])
            ->where([
                    ['NodoResponsable','!=','Etapa proyecto'],
                ])

            ->get();

        }


        return $alerts;

    }


    public function getAllAlert($clients){

        $alerts = $this->whereNull('Atendida');


        if(isset($clients)){

            $alerts->where(function ($query) use ($clients){
                foreach($clients as $client){
                    $query->where('NodoCliente', '!=',$client);
                }
            });

        }


             $alerts = $alerts->select(['NodoCliente', 'AlertActiveID'])
            ->take(1000)->orderBy('Fecha','desc')
            ->where('Mensaje','!=','_CA - Alert me when the free space of a volume is less than 15% New')
            ->where('Mensaje','!=','_CA - Custom alerts of warning volume was triggered.')
            ->where('Mensaje','!=','_CA - Alert me when an application is in unknown condition')
            ->where('CICaption','!=','Windows Time Skew')
            ->where('CICaption','!=','Monitoreo detect_process')
            ->where([
                    ['NodoResponsable','!=','Etapa proyecto'],
                ])
            ->where([
                    ['NodoResponsable','!=','Cliente'],
                    ['CIResponsable','!=','Cliente'],
                ])


            ->get();


            return json_decode($alerts);

    }

    public function countAlert($clientes){

        $alerts = $this->whereNull('Atendida');


        if(isset($clients)){

            $alerts->where(function ($query) use ($clients){
                foreach($clients as $client){
                    $query->where('NodoCliente', '!=',$client);
                }
            });

        }


             $alerts = $alerts
            ->where('Mensaje','!=','_CA - Alert me when the free space of a volume is less than 15% New')
            ->where('Mensaje','!=','_CA - Custom alerts of warning volume was triggered.')
            ->where('Mensaje','!=','_CA - Alert me when an application is in unknown condition')
            ->where('CICaption','!=','Windows Time Skew')
            ->where('CICaption','!=','Monitoreo detect_process')
            ->where([
                    ['NodoResponsable','!=','Etapa proyecto'],
                ])
            ->where([
                    ['NodoResponsable','!=','Cliente'],
                    ['CIResponsable','!=','Cliente'],
                ])


            ->count();


            return json_decode($alerts);

    }


    public function getByClient($client){

        return $this->where('Mensaje', 'LIKE', $client.'%');

    }

       //metodo el cual recibira el dato enviado desde ticketController.php y devolvera un resultado limpio -- NEW !
     public static function getDataBody($data){

        $xml = simplexml_load_string($data);

        $lines = explode( PHP_EOL , $xml->sourceMessage->msgProperty[4]);

        foreach ($lines as $line) {

            if( strlen($line) > 1){

                if(strpos($line, ':')){

                    $words = explode(":", $line, 2);

                    $array[$words[0]] = $words[1];

                }
                else{
                    $array[] = $line;
                }
            }
        }

        return $array;
    }

     public static function searchDown($Wordtoseek)
    {
        $word1 = 'down';
        $word2 = 'Down';
        $resultado1 = strpos($Wordtoseek, $word1);
        $resultado2 = strpos($Wordtoseek, $word2);

       $return = false;

        if ($resultado1 > 1  || $resultado2 > 1 )
        {
            $return = true;
        }

        return $return;
    }




}
