<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Ticket
 * @package App\Models
 */
class Ticket extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tickets';
    protected $guarded = ['id'];



    
   	public static function generateOnDb($alert, $ticket){


   		$data = [
					'alert_id'          => $alert->AlertActiveID,
					'client'            => $alert->NodoCliente,
					'system'            => $ticket['system'],
					'system_id'         => $ticket['system_id'],
					'alert_time_raised' => $alert->Fecha,
					'alertObjectID'     => $alert->AlertObjectID,
				    'metodo'            => $ticket['gestion'],
   		];

   		$ticket = self::create($data);
			

   		
   		return $ticket;

   	}
}
