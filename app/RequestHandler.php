<?php

namespace App;

/**
 * This class create an array ready to send to method creatorHandle
 */
class RequestHandler {

	public $sid;
	public $creatorHandle;
	public $afectado;
	public $pcat;
	public $report;

	public function __construct($sid, $creatorHandle,$afectado,$pcat){
		$this->sid = $sid;
		$this->creatorHandle = $creatorHandle;
		$this->afectado = $afectado;
		$this->pcat = $pcat;
		$this->report = "rptmeth:7304";
	}

	/**
	 * This function return an array with data to send to ticket
	 * @param  array $data [description]
	 * @return array       [description]
	 */
	public function get($data){

		$request = [
			'sid'           => $this->sid,
			'creatorHandle' => $this->creatorHandle,
			'attrVals' => [
				'string' => [
					'customer',
					$this->afectado,
					'category',
					'pcat:'.$this->pcat,
					'type',
					'R',
					'zreporting_met',
					'7304',
					'description',
					$data,

					]
			],
			'propertyValues' => '',
			'template' => '',
			'attributes' => '',
			'newRequestHandle' => '',
			'newRequestNumber' => '',

		];

		return $request;
	}

	public function incident($data){

		$request = [
			'sid'           => $this->sid,
			'creatorHandle' => $this->creatorHandle,
			'attrVals' => [
				'string' => [
					'customer',
					$this->afectado,
					'category',
					'pcat:'.$this->pcat,
					'type',
					'I',
					'zreporting_met',
					'7304',
					'description',
					$data,
					]
			],
			'propertyValues' => '',
			'template' => '',
			'attributes' => '',
			'newRequestHandle' => '',
			'newRequestNumber' => '',

		];

		return $request;
	}

}
