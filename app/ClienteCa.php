<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteCa extends Model
{
  protected $connection = 'mysql';
  protected $table = 'clientesCA';
  protected $guarded = ['id'];
  public $timestamps = false;
}
