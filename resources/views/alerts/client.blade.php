@extends('master')
@section('content')
@section('color','#FFF0F5')
@include('alerts._alert_modal')
<div  class="container">
	<div class="col-md-12">
		<h3 style="color:#20B2AA">Alertas activas de responsabilidad cliente</h3>
		<h4 style="color:#20B2AA" id='spam'></h4>
	</div>

</div>


<div class="container">
	<div>	
		<div class="row margin-top">
			
			<div class="btn-group" role="group" >
				<button type="button" id="button"  value="cliente" class="filter btn btn-warning">Alertas Cliente</button>
	  		</div>
		</div>
		
	</div>
</div>


<div class="container-fluid">
	<div class="row margin-top">
		<div class="col-md-12">
			<table class = "table display table-bordered" id = "alerts">
				<thead>
					<th class="col-md-1">Cliente</th>
					<th class="col-md-1">Message</th>
					<th class="col-md-1">Hora Alerta</th>
					<th class="col-md-1">IP</th>
					<th class="col-md-4">Nodo</th>
					<th class="col-md-1">Responsable Alarma</th>
					<th class="col-md-1">Responsable Nodo</th>
					<th class="col-md-1">Poller</th>
					<th class="col-md-1">Acciones</th>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div>
	</div>

</div>

<div class="container">
	<div class="row margin-top">
		<div class="col-md-12">
			<a href="/alertsdashboard" class = "btn btn-success">Alertas</a>
			<a class = "btn btn-primary" href="/tickets">Tickets</a>
			<a href="/tickets/search" class = "btn btn-primary">Buscar Tickets</a>
		</div>			
	</div>
</div>
<script>
				var countdownfrom=100000;
				var currentsecond=countdownfrom+1;
				var today = new Date();
				


			 function countredirect()
			 {
				var today = new Date();
				var dt = new Date();	
			    if (currentsecond!=1)
			    {	
			    	
			    	var dd = today.getDate();
					var mm = today.getMonth()+1; //January is 0!
					var yyyy = today.getFullYear();
					today =    dd+'/'+mm+'/'+yyyy; 

			    	
			    	
			        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
				    currentsecond-=1
				    	
						$("#button").click();
						$("#spam").html("Ultima actualización: "+today +"-"+ time);


				}else{
						
						window.location=targetURL
						return
					}


					setTimeout("countredirect()",60000)
					countdownfrom -1;
			  }

					countredirect() 

		</script>
@endsection