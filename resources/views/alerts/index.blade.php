@extends('master')
@section('content')
@include('alerts._alert_modal')
<div class="container">
	<div class="col-md-12">
		<h3>Alertas Automáticas.</h3>
		<h4 id='spam'></h4>
	</div>


	<div class="row margin-top">
		<div class="btn-group" role="group" >
			<button type="button" id="button"  value="2" class="filter btn btn-warning">ALERTAS NOTIFICADAS</button>
	  		
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row margin-top">
		<div class="col-md-12">
			<table class = "table display table-bordered" id = "alerts">
				<thead>
					<th class="col-md-1">Cliente</th>
					<th class="col-md-1">Message</th>
					<th class="col-md-1">Hora Alerta</th>
					<th class="col-md-1">IP</th>
					<th class="col-md-4">Nodo</th>
					<th class="col-md-1">Responsable Alarma</th>
					<th class="col-md-1">Responsable Nodo</th>
					<th class="col-md-1">Poller</th>
					<th class="col-md-1">Acciones</th>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div>
	</div>

</div>

<div class="container">
	<div class="row margin-top">
		<div class="col-md-12">
			<a class = "btn btn-primary" href="/tickets">Tickets</a>
			<a href="/tickets/search" class = "btn btn-primary">Buscar Tickets</a>
			<a href="/clientes" class=" btn btn-primary">Gestionar Clientes</a>
		</div>			
	</div>
</div>

<script>
				var countdownfrom=100000;
				var currentsecond=countdownfrom+1;
				var today = new Date();
				


			 function countredirect()
			 {
				var today = new Date();
				var dt = new Date();	
			    if (currentsecond!=1)
			    {	
			    	
			    	var dd = today.getDate();
					var mm = today.getMonth()+1; //January is 0!
					var yyyy = today.getFullYear();
					today =    dd+'/'+mm+'/'+yyyy; 

			    	
			    	
			        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
				    currentsecond-=1
				    	
						$("#button").click();
						$("#spam").html("Ultima actualización: "+today +"-"+ time);


				}else{
						
						window.location=targetURL
						return
					}


					setTimeout("countredirect()",60000)
					countdownfrom -1;
			  }

					countredirect() 

		</script>
@endsection