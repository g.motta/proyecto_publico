<!DOCTYPE html>
<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        
        <title>Solarwind Alertas</title>
        <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>        
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
         <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel='stylesheet' type='text/css'>

        <style>
            html, body {
                height: 100%;
            }

            body {
                font-size: 14px !important;
                font-weight: 100;
                font-family: 'Lato';
            }

            
            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }

            .margin-top{
                margin-top: 20px;
            }


            
        </style>

        <script type="text/javascript" src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jszip.min.js')}}"></script>
       
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <link href='/sweetalert.css' rel='stylesheet' type='text/css'>        

        <script src="/sweetalert.min.js"></script>

        <script type="text/javascript" src="{{ asset('DataTables/media/js/jquery.dataTables.js') }}"></script>
        <script type="text/javascript" src="{{ asset('DataTables/media/js/dataTables.bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('DataTables/extensions/Buttons/js/buttons.bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('DataTables/extensions/Buttons/js/buttons.html5.js') }}"></script>

        <link rel="stylesheet" href="{{ asset('DataTables/media/css/dataTables.bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('DataTables/media/css/dataTables.material.css') }}">
        
        
        <script src="/js/alertsTableCtrl.js"></script>
        <script src="/js/ticketsTableCtrl.js"></script>
        <script src="/js/stateButtonsCtrl.js"></script>
        <script src="/js/detailsButtonCtrl.js"></script>
        <script src="/js/createTicketButtonCtrl.js"></script>
        <script src="/js/viewAlertsCtrl.js"></script>



    </head>
    <body style="background: @yield('color')">
        <div class="">

                @yield('content')

        </div>
    </body>
</html>
