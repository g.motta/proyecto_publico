@extends('tickets.search')


@section('contenido')

	@foreach ($AlertHistory as $AlertHistory)
		<tr>
			<td>{{ $AlertHistory->system_id}}</td>
			<td>{{ $AlertHistory->client}}</td>
			<td>{{ $AlertHistory->Nodo}}</td>
			<td>{{ $AlertHistory->ip}}</td>
			@if(!is_null($AlertHistory->resumen))
			<td>{{ $AlertHistory->resumen}}</td>
			@else
			<td>{{ $AlertHistory->mensaje}}</td>
			@endif
			<td>{{ $AlertHistory->grupoCi}}</td>
			<td>{{ $AlertHistory->grupoNodo}}</td>
			<td>{{ $AlertHistory->alert_time_raised}}</td>
			<td>{{ $AlertHistory->created_at}}</td>
			<td>{{ $AlertHistory->alertObjectID}}</td>
			<td>{{ $AlertHistory->systema}}</td>
			<td>{{ $AlertHistory->alert_id}}</td>
		</tr>
	@endforeach
@endsection