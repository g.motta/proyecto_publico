<!DOCTYPE html>
<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        
        <title>Solarwind CA</title>
        <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>        
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
         <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel='stylesheet' type='text/css'>

        <style>
            html, body {
                height: 100%;
            }

            body {
                font-size: 14px !important;
                font-weight: 100;
                font-family: 'Lato';
            }

            
            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }

            .margin-top{
                margin-top: 20px;
            }
           	

            
        </style>
        
       	<script type="text/javascript" src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jszip.min.js')}}"></script>
        

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <link href='/sweetalert.css' rel='stylesheet' type='text/css'>        

    	<script type="text/javascript" src="{{ asset('DataTables/media/js/jquery.dataTables.js') }}"></script>
        <script type="text/javascript" src="{{ asset('DataTables/media/js/dataTables.bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('DataTables/extensions/Buttons/js/buttons.html5.js') }}"></script>
       	<link rel="stylesheet" href="{{ asset('DataTables/extensions/Buttons/css/buttons.datatables.css') }}">
       	<link rel="stylesheet" href="{{ asset('DataTables/media/css/dataTables.bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('DataTables/media/css/dataTables.material.css') }}">


        
    </head>
    <body>

    		

        <div class="container">

			
				  <div class="panel panel-default">
				  			<div class="panel-heading">Buscador de Tickets</div>
				  	<div class="panel-body">
						{!! Form::open(['url' => '/ticket/viewSearch', 'method' => 'GET']) !!}
				    		
				    		<div class="form-group">
				    			{!! Form::label('Desde','Desde:')!!}
				    		 	<input type="date" name="fechaDesde" id="fechasDesde" required="">
				    		 	{!! Form::label('Fecha',' Hasta:')!!}
				    		 	<input type="date" name="fechaHasta" id="fechaHasta" >

				    		</div>

				    		<div class="form-group">

								{!! Form::label('Buscar Por:','Buscar Por:') !!}   	


				    			{!! Form::select('choise', array('ip' => 'IP', 'Nodo' => 'Nodo ID' , 'alert_id' => 'Id de la alerta','system_id'=>'Os/Ticket', 'client'=>'Nombre del cliente' )) !!}

				    			{!! Form::label('Buscar Por:','Valor:') !!}  
				    			{!! Form::text('choiseSeleccion',null,['class'=> 'form-control,col-xs-2' , 'placeholder' => ''])!!}
				    		 
				    		</div>
			    	</div>	
			      </div>	
			    		<div class="form-group">
			    		 	
			    		 	{!! Form::submit('Buscar',['class' => 'btn-primary']) !!}
			    		 	
			    		</div>
			    	
		</div>
		    			{!! Form::close() !!}
		
		 <div clas="container-fluid">
				<div class="row margin-top">
					<div class="col-md-12">
						<table class="table display table-bordered" id="example">
							<thead>
							  <tr>
								<td class="col-md-1">Os/ticket</td>
								<td class="col-md-1">Cliente</td>
								<td class="col-md-1">Nodo</td>
								<td class="col-md-1">Ip</td>
								<td class="col-md-5">Detalle de alerta</td>
								<td class="col-md-1">Ci Resp</td>
								<td class="col-md-1">Nodo Resp</td>
								<td class="col-md-1">Fecha notificación</td>
								<td class="col-md-1">Fecha Gestionada</td>
								<td class="col-md-1">Fecha atendida Moebius</td>
								<td class="col-md-1">Herramienta reporte</td>
								<td class="col-md-1">iD Alerta</td>
							  </tr>
							</thead>
							<tbody>
								@yield('contenido')
							</tbody>
						</table>
					</div>
				</div>

				<div class="row margin-top">
					<div class="col-md-12">
						<a href="/alertsdashboard" class = "btn btn-success">Alertas</a>
						<a class = "btn btn-primary" href="/tickets">Tickets</a>
						<a href="/clientes" class=" btn btn-primary">Gestionar Clientes</a>
					</div>
					
				</div>
		</div>		
        
    </body>
</html>

<script>
$(document).ready( function() {


$('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [ {
            extend: 'excelHtml5',
            customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 
                $('row c[r^="C"]', sheet).attr( 's', '2' );
            }
        } ]
    } );
} );
</script>
