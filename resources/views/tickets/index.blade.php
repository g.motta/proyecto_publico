@extends('master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<h1>Tickets Creados</h1>
	</div>
</div>
<div class="row margin-top">
	<div class="col-md-12">
		@include('tickets._table')
	</div>
	
</div>

<div class="row margin-top">
	<div class="col-md-12">
		<a href="/alertsdashboard" class = "btn btn-success">Alertas</a>
		<a href="/tickets/search" class = "btn btn-primary">Buscar Tickets</a>
		<a href="/clientes" class=" btn btn-primary">Gestionar Clientes</a>
	</div>
	
</div>
@endsection