$(document).ready(function(){

	getDetailsAlert = function(AlertActiveID){
		
		var cells = '<table class = "table table-bordered details">';

		$.get( '/alerts/get/' + AlertActiveID  , function( data ) {			

		}).done(function(data) {

    	$.each(data, function(idx, elem){

				cells += '<tr><td><b>' + idx + '</b></td><td>' + elem + '</td></tr>';

			});
  		  
  		var table = [
		 	cells,
			'</table>']
			.join('');
			
			$(".modal-body").empty();
			$('#myModal').modal('show');
			$(".modal-body").append(table);

			$("#createTicket").attr('onClick', 'createTicket(' + AlertActiveID + ')');


		});
	};
});