$(document).ready(function() {
  

  
  var t = $('#tickets').DataTable({
    "columnDefs": [
    ]
  });

    var query = '/tickets/all';

		$.get( query , function( data ) {
  		
    	$.each(data, function(idx, elem){
				
				if(	elem.metodo == 1){
					mensaje = "Automático";
				}else{
					mensaje = "Manual";
				}
				
				t.row.add([
					elem.alert_id,
					elem.client,
					elem.system_id,
					elem.system,
					elem.alert_time_raised,
					elem.created_at,
					mensaje
				]).draw();
			});
    });
});