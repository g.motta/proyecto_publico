$(document).ready(function() {
    $('#alerts').DataTable({
    	"order": [[ 4, "desc" ]],
    	  "columnDefs": [
					{ "width": "350px", "targets": 1 },
					{ "width": "100px", "targets": 2 },
					{ "width": "100px", "targets": 3 },
    	  	{ "width": "120px", "targets": 4 },
      		{ "width": "100px", "targets": 5 },    		
    		]
    		});
} );
