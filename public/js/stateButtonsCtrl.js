$( document ).ready(function() {

	var table = $("#alerts tbody");
  var t = $('#alerts').DataTable();


	$( ".filter" ).click(function() {
		swal({   
			title: "Cargando...",      
			imageUrl: "/images/ripple.svg",
			showConfirmButton: false  
		});

		t.clear().draw();

		var clientName = $("input[name=clientName]");

		var query = '/alerts/' + this.value;

		if(clientName.val() != 'undefined') query = query +  '/' + clientName.val();

		$.get( query , function( data ) {
  		
    	$.each(data, function(idx, elem){
				var estado;
				if(elem.Severidad == 1)
				{

					estado = "Warning";
				}else
				{

					estado = "Critico";
				}

				console.log(data);
				
				t.row.add([
					elem.NodoCliente,
					elem.CICaption + "  "+ elem.Mensaje,
					elem.Fecha,
					elem.IP,
					elem.NodoCaption,
					elem.CIResponsable,
					elem.NodoResponsable,
					elem.NodoPoller,
					'<td><a type="button" class="btn btn-default" onClick = "getDetailsAlert('+ elem.AlertActiveID +')"  id = "details" value = ' + elem.AlertActiveID + '><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a> </button></td>'
				]).draw();
			
			   	
  		});
  		
  		swal({
			  title: "Alertas Cargadas",
			  text: "Listo para usar",
			  timer: 500,
			  showConfirmButton: false
			});
  				
		});
	});
});
