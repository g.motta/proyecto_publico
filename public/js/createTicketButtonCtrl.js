$(document).ready(function() {
    createTicket = function(id){
		
		swal({   
			title: "Generando Ticket",      
			imageUrl: "/images/ripple.svg",
			showConfirmButton: false  
		});

			var token = $('meta[name="csrf-token"]').attr('content');

			$.post('/ticket/create', {id: id, _token: token}, function(data, textStatus, xhr) {
				
			if(data != ""){
				swal({  
				 title: "Ticket Generado exitosamente",  
				  text: "Su numero de ticket es : "+data,  
				   type: "success",   
				    showLoaderOnConfirm: true, }, 
				    function()
				    {setTimeout(function()
				    	{ location.reload(1);}); });

			}else
			{
				swal({  
				 title: "Ticket no Generado",  
				  text: "Se detecto un error al momento de generar el ticket.",  
				   type: "warning",   
				    showLoaderOnConfirm: true, }, 
				    function()
				    {setTimeout(function()
				    	{ location.reload(1);}); });


			}




			});


    }
    
} );